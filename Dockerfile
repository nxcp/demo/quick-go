FROM golang:alpine as builder
WORKDIR /build 
ADD . .
RUN go build -o main .

FROM alpine
RUN adduser -S -D -H -h /app appadm
USER appadm
COPY --from=builder /build/main /app/
ENV GIN_MODE release
WORKDIR /app
EXPOSE 8080
CMD ["./main"]