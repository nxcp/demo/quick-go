package main

import (
	"net/http"
	"os"

	"github.com/gin-gonic/gin"
)

func main() {
	app := gin.New()
	app.GET("/", func(c *gin.Context) {
		c.JSON(http.StatusOK, "ok"+os.Getenv("message"))
	})

	app.Run(":8080")
}
